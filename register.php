<?php
session_start();
require 'auth/auth.php';

if (!isset($_SESSION["login"])) {
    header("location:index2.php");
    exit;
}

if (isset($_POST["register"])) {

    if (registrasi($_POST) > 0) {
        echo "<script>confirm('Tambah admin berhasi !!');
		window.location = 'pages/tables/admin.php';</script>";
    } else {
        echo mysqli_error($conn);
    }
}

?>

<!doctype html>
<html lang="en">

<head>
    <title>CRUD Sederhana</title>
    <link href='https://cdn-icons-png.flaticon.com/512/148/148848.png' rel='shortcut icon'>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="css/style.css">

</head>

<body>
    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 text-center mb-5">

                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-7 col-lg-5">
                    <div class="login-wrap p-4 p-md-5">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-user-o"></span>
                        </div>
                        <h3 class="text-center mb-4">Tambah Admin</h3>

                        <form action="" method="POST" class="login-form">
                            <div class="form-group">
                                <input type="text" name="username" class="form-control rounded-left" placeholder="Username" required>
                            </div>
                            <div class="form-group d-flex">
                                <input type="password" name="password" class="form-control rounded-left" placeholder="Password" required>
                            </div>
                            <div class="form-group d-flex">
                                <input type="password" name="password2" class="form-control rounded-left" placeholder="Ulangi Password" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="form-control btn btn-primary rounded submit px-3" name="register">Register</button>
                            </div>

                    </div>
                    </form>
                    <form action="index.php" method="" class="login-form">
                        <div class="form-group">
                            <button type="submit" class="form-control btn btn-primary rounded submit px-3">Dashboard</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </section>

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

</body>

</html>