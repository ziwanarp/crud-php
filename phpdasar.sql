-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 21 Agu 2022 pada 17.45
-- Versi server: 10.4.20-MariaDB
-- Versi PHP: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpdasar`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `nrp` char(11) NOT NULL,
  `nama` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `jurusan` varchar(64) NOT NULL,
  `gambar` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `nrp`, `nama`, `email`, `jurusan`, `gambar`) VALUES
(1, '20170910084', 'Ziwana Rizwan Pramudia', 'ziwanarizwan@gmail.com', 'Sistem Informasi', 'ziwana.png'),
(5, '20170910099', 'Radea Andre Kumara', 'radea@gmail.com', 'Sistem Informasi', 'radea.jpg'),
(6, '2070910085', 'Rizkian Pramudia', 'rizkianp@gmail.com', 'Teknik Mesin Motor', 'rizkian.jpg'),
(9, '20170910001', 'Anggarha Rizkiawan Pramudia', 'anggarha@gmail.com', 'Pendidikan Bahasa Sunda', '62d62f4d92379.png'),
(15, '20180910084', 'Riki Afrizan', 'riki@gmail.com', 'Pendidikan Bahasa Rusia', '62e8cd1120823.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(25, 'admin', '$2y$10$W2l8VWpK5oDp2PycWVZpz.KI0D/qh8wMePu17ymmNADCK.FA7TXjO'),
(36, 'admin2', '$2y$10$NnRWhEzQaUTB1dpH25PMYeSBQ2P4pfbqgmcTSfUdysbR29CyVPjCi'),
(37, 'admin3', '$2y$10$e8wpc.qx0zahk/jPURqyheF4v6iegifOebmRfbCFUAo0HWGduptNq'),
(38, 'admin4', '$2y$10$YEliCeLdNfoQDn2C8xrjyu96HOHv76208dCdMOk0HTj3mgShoubx6'),
(39, 'admin5', '$2y$10$/jCg/lMt05vZStlZCW615u4UyUJf57IKMLtTAxgiEXfRr1B86N6RG');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
