<?php
session_start();
require 'auth/auth.php';

// cek cookie
if (isset($_COOKIE['id']) && isset($_COOKIE['key'])) {
  $id = $_COOKIE['id'];
  $key = $_COOKIE['key'];

  //ambil username berdasarkan id
  $result = mysqli_query($conn, "SELECT username FROM user WHERE id = $id");
  $row = mysqli_fetch_assoc($result);

  // cek cookie dan username
  if ($key === hash('sha256', $row['username'])) {
    $_SESSION['login'] = true;
  }
}

//kembalikan ke index jika masih ada session
if (isset($_SESSION["login"])) {
  header("location:index.php");
  exit;
}



if (isset($_POST["login"])) {

  $username = $_POST["username"];
  $password = $_POST["password"];

  $result = mysqli_query($conn, "SELECT * FROM user WHERE username = '$username'");

  //cek username
  if (mysqli_num_rows($result) === 1) {

    // cek password
    $row = mysqli_fetch_assoc($result);
    if (password_verify($password, $row["password"])) {

      //set session
      $_SESSION["login"] = true;


      //check remember me
      if (isset($_POST['remember'])) {
        // buat cookie

        setcookie('id', $row['id'], time() + 60);
        setcookie('key', hash('sha256', $row['username']), time() + 60);
      }


      header("location: index.php");
      exit;
    }
  }

  $error = true;
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="css/style2.css" />
  <title>Login Admin</title>
</head>

<body>
  <div class="container">
    <h1>Login Admin</h1>

    <?php if (isset($error)) : ?>
      <p class="text-center mb-4" style="color: yellow; ">Username / Password Salah !!! </p>
    <?php endif; ?>

    <form action="" method="POST">
      <div class="form-control">
        <input type="text" name="username" required autocomplete="off">
        <label>Username</label>
      </div>

      <div class="form-control">
        <input type="password" required name="password">
        <label>Password</label>
      </div>

      <button class="btn" type="submit" name="login">Login</button>
      <p><label>Remember Me<input type="checkbox" id="remember" name="remember"></label></p>
    </form>
  </div>
  <script src="js/script.js"></script>
</body>

</html>