<?php

session_start();
require '../../auth/auth.php';

if (!isset($_SESSION["login"])) {
	header("location: ../../login.php");
	exit;
}


$id = $_GET["id"];

if (hapusadm($id) > 0) {
	echo " 
		<script>
		alert('data admin berhasil dihapus');
		document.location.href = 'admin.php';
		</script>
		";
} else {
	echo " 
		<script>
		alert('data admin gagal dihapus');
		document.location.href = 'admin.php';
		</script>
		";
}
