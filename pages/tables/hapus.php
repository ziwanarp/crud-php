<?php
session_start();
require '../../auth/auth.php';

if (!isset($_SESSION["login"])) {
	header("location: ../../login.php");
	exit;
}

$id = $_GET["id"];

if (hapus($id) > 0) {
	echo " 
		<script>
		alert('data berhasil dihapus');
		document.location.href = 'data.php';
		</script>
		";
} else {
	echo " 
		<script>
		alert('data gagal dihapus');
		document.location.href = 'data.php';
		</script>
		";
}
