<?php

session_start();
require '../../auth/auth.php';

if (!isset($_SESSION["login"])) {
  header("location:../../index2.php");
  exit;
}




//ambil data di url
$id = $_GET["id"];
// query data mahasiswa berdasarkan id
$mhs = query("SELECT * FROM mahasiswa WHERE id = $id")[0];

// cek apakah tombol submit sudah ditekan ?
if (isset($_POST['submit'])) {

  // cek apakah data berhasil diubah ?
  if (update($_POST) > 0) {
    echo " 
		<script>
		alert('data berhasil diubah');
		document.location.href = 'data.php';
		</script>
		";
  } else {
    echo " 
		<script>
		alert('data gagal diubah');
		document.location.href = 'data.php';
		</script>
		";
  }
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>CRUD Sederhana</title>
  <link href='https://cdn-icons-png.flaticon.com/512/148/148848.png' rel='shortcut icon'>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper">


    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block">Admin</a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item">
              <a href="../../index.php" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                  Dashboard
                </p>
              </a>
            <li class="nav-item">
              <a href="../forms/general.php" class="nav-link">
                <i class="nav-icon fas fa-edit"></i>
                <p>
                  Tambah Data Mahasiswa
                </p>
              </a>
            <li class="nav-item">
              <a href="../../register.php" class="nav-link">
                <i class="nav-icon fas fa-chalkboard-teacher"></i>

                <p>
                  Tambah Admin
                </p>
              </a>

            <li class="nav-item">
              <a href="data.php" class="nav-link">
                <i class="nav-icon fas fa-table"></i>
                <p>
                  Data Mahasiswa
                </p>
              </a>

            <li class="nav-item">
              <a href="../../logout.php" class="nav-link">
                <p>
                  logout
                </p>
              </a>
            </li>
          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Form Update Data</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="../../index.php">Home</a></li>
                <li class="breadcrumb-item active">Form Update Data</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Update Data</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="" method="post" enctype="multipart/form-data">
                  <div class="card-body">
                    <div class="form-group">
                      <input type="hidden" name="id" value="<?= $mhs["id"]  ?>">
                      <input type="hidden" name="gambarlama" value="<?= $mhs["gambar"]  ?>">
                      <label for="exampleInputNrp">NRP</label>
                      <input type="text" class="form-control" id="exampleInputNrp" placeholder="Masukan NRP" name="nrp" value="<?= $mhs["nrp"]; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputName">Nama</label>
                      <input type="text" class="form-control" id="exampleInputName" placeholder="Masukan Nama" name="nama" value="<?= $mhs["nama"]; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Masukan email" name="email" value="<?= $mhs["email"]; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputJurusan">Jurusan</label>
                      <input type="text" class="form-control" id="exampleInputJurusan" placeholder="Masukan Jurusan" name="jurusan" value="<?= $mhs["jurusan"]; ?>">
                    </div>
                    <div class="form-group">
                      <img src="../../img/<?= $mhs["gambar"]; ?>" width="80">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">Upload Gambar</label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="exampleInputFile" name="gambar">
                          <label class="custom-file-label" for="exampleInputFile">Pilih Gambar</label>
                        </div>

                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->


            </div>

          </div>
          <!-- /.card -->
        </div>
        <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
  </div>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="../../plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- bs-custom-file-input -->
  <script src="../../plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
  <!-- AdminLTE App -->
  <!-- Page specific script -->
  <script>
    $(function() {
      bsCustomFileInput.init();
    });
  </script>
</body>

</html>