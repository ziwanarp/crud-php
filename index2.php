<?php
session_start();
require 'auth/auth.php';

if (isset($_SESSION["login"])) {
    header("location:index.php");
    exit;
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css/style3.css" />
    <title>CRUD Sederhana
    </title>
</head>

<body>
    <div class="container">
        <div class="split left">
            <h1>Admin</h1>
            <a href="login.php" class="btn">Login</a>
        </div>
        <div class="split right">
            <h1>Pengunjung</h1>
            <a href="pages/tables/viewer.php" class="btn">Login</a>
        </div>
    </div>

    <script src="js/script2.js"></script>
</body>

</html>