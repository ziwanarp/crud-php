<?php

// koneksi database
$conn = mysqli_connect("localhost", "root", "", "phpdasar");




function query($query)
{
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}




function tambah($data)
{
    global $conn;
    $nrp = htmlspecialchars($data["nrp"]);
    $nama = htmlspecialchars($data["nama"]);
    $email = htmlspecialchars($data["email"]);
    $jurusan = htmlspecialchars($data["jurusan"]);

    // upload gambar
    $gambar = upload();
    if ($gambar == false) {
        return false;
    }

    //query insert data
    $query = "INSERT INTO mahasiswa VALUES ('','$nrp','$nama','$email','$jurusan','$gambar')";
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}




function upload()
{

    $namafile = $_FILES['gambar']['name'];
    $ukuranfile = $_FILES['gambar']['size'];
    $error = $_FILES['gambar']['error'];
    $tmpname = $_FILES['gambar']['tmp_name'];

    //cek apakah ada gambar yang di upload
    if ($error === 4) {
        echo "
		<script>
		alert('pilih gambar dulu');
		</script>
		";
        return false;
    }

    //cek apakah yang di upload adalah gambar
    $ekstensigambar = ['jpg', 'jpeg', 'png', 'JPEG'];
    $ekstensi = explode('.', $namafile);
    $ekstensi = strtolower(end($ekstensi));
    if (!in_array($ekstensi, $ekstensigambar)) {
        echo "
		<script>
		alert('yang di upload bukan gambar');
		</script>
		";
        return false;
    }
    // cek jiga ukuran gbr terlalu besar
    if ($ukuranfile > 2000000) {
        echo "
		<script>
		alert('ukuran gambar harus dibawah 2 mb');
		</script>
		";
        return false;
    }
    // generate nama file
    $namafilebaru = uniqid();
    $namafilebaru .= '.';
    $namafilebaru .= $ekstensi;
    // lolos cek gambar siap upload
    move_uploaded_file($tmpname, '../../img/' . $namafilebaru);

    return $namafilebaru;
}




function hapus($id)
{
    global $conn;
    mysqli_query($conn, "DELETE FROM mahasiswa Where id = $id");

    return mysqli_affected_rows($conn);
}

function hapusadm($id)
{
    global $conn;
    mysqli_query($conn, "DELETE FROM user Where id = $id");

    return mysqli_affected_rows($conn);
}




function update($data)
{
    global $conn;
    $id = $data["id"];
    $nrp = htmlspecialchars($data["nrp"]);
    $nama = htmlspecialchars($data["nama"]);
    $email = htmlspecialchars($data["email"]);
    $jurusan = htmlspecialchars($data["jurusan"]);
    $gambarlama = htmlspecialchars($data["gambarlama"]);


    // cek apakah user upload gambar baru
    if ($_FILES['gambar']['error'] === 4) {
        $gambar = $gambarlama;
    } else {
        $gambar = upload();
    }

    //query insert data
    $query = "UPDATE mahasiswa SET nrp = '$nrp', nama = '$nama', email = '$email', jurusan = '$jurusan', gambar = '$gambar' WHERE id = $id";
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function forgot($data1)
{
    global $conn;

    $username = strtolower(stripslashes($data1["username"]));
    $password = mysqli_real_escape_string($conn, $data1["password"]);
    $password2 = mysqli_real_escape_string($conn, $data1["password2"]);

    //cek username sudah ada atau tidak
    $result = mysqli_query($conn, "SELECT username FROM user WHERE username = '$username'");

    if (mysqli_fetch_assoc($result) !== 1) {
        echo "<script>
		alert('Username Tidak ada!');</script>";

        return false;

        if ($password !== $password2) {
            echo "<script> 
            alert('Confirm Password tisak sesuai!');
            </script>";
            return false;
        }
    }



    //enkripsi password
    $password = password_hash($password, PASSWORD_DEFAULT);


    //tambah user baru ke database
    $query = "UPDATE user SET password = '$password' WHERE username = $username";
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}




function cari($keyword)
{

    $query = "SELECT * FROM mahasiswa WHERE nama LIKE'%$keyword%' OR nrp LIKE'%$keyword%' OR email LIKE'%$keyword%' OR jurusan LIKE'%$keyword%' ";
    return query($query);
}




function registrasi($data)
{
    global $conn;

    $username = strtolower(stripslashes($data["username"]));
    $password = mysqli_real_escape_string($conn, $data["password"]);
    $password2 = mysqli_real_escape_string($conn, $data["password2"]);

    //cek username sudah ada atau tidak
    $result = mysqli_query($conn, "SELECT username FROM user WHERE username = '$username'");

    if (mysqli_fetch_assoc($result)) {
        echo "<script>
		alert('Username Sudah ada!');</script>";

        return false;
    }

    //cek confirmasi password
    if ($password !== $password2) {
        echo "<script> 
		alert('Confirm Password tisak sesuai!');
		</script>";
        return false;
    }
    //enkripsi password
    $password = password_hash($password, PASSWORD_DEFAULT);


    //tambah user baru ke database
    mysqli_query($conn, "INSERT INTO user VALUES('','$username','$password')");

    return mysqli_affected_rows($conn);
}
